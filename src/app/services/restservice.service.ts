import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToDoType } from '../types/ToDoType';

@Injectable({
  providedIn: 'root'
})
export class RESTserviceService {

  constructor(private http: HttpClient) { }

  public getToDos(): Observable<ToDoType[]> {
    return this.http.get<ToDoType[]>('https://jsonplaceholder.typicode.com/todos');
  }

}
