import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.title = 'angular-practice-project';
    this.isActive = true;
    this.items = [{ name: 'item 1 name', value: 'item 1 value' }, { name: 'item 2 name', value: 'item 2 value' }];
  }
  title: String;
  isActive: Boolean;
  items = [];
  public onClik(event: MouseEvent) {
    alert('Event happened\n' + event.clientX);
  }
}
