import { Injectable } from '@angular/core';
import { DogType } from '../types/DogType';

@Injectable({
  providedIn: 'root'
})
export class DogService {

  private dogs: DogType[];

  constructor() {
    this.dogs = [
      { name: 'Lily', age: 5 },
      { name: 'Crabby', age: 5 },
      { name: 'Crusty', age: 5 },
      { name: 'Rex', age: 5 }
    ];
  }

  getDogs(): DogType[] {
    return this.dogs;
  }

  getDog(name: String): DogType {
    return this.dogs.filter(d => name === d.name)[0];
  }

  public addDog(dog: DogType) {
    this.dogs.push({
      name: dog.name,
      age: dog.age
    })
  }

}
