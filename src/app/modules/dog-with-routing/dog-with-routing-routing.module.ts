import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DogByIdComponent } from './components/dog-by-id/dog-by-id.component';
import { DogComponent } from './components/dog/dog.component';

const routes: Routes = [
  {
    path: 'dogs',
    component: DogComponent
  },
  {
    path: 'dogs/:name',
    component: DogByIdComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DogWithRoutingRoutingModule { }
