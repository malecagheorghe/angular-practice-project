import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDogComponent } from './components/add-dog/add-dog.component';
import { DogComponent } from './components/dog/dog.component';
import { PresenterChildComponent } from './components/presenter-child/presenter-child.component';

import { DogWithRoutingRoutingModule } from './dog-with-routing-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DogComponent,
    PresenterChildComponent,
    AddDogComponent
  ],
  imports: [
    CommonModule,
    DogWithRoutingRoutingModule,
    FormsModule
  ],
  exports: [
    DogComponent
  ]
})
export class DogWithRoutingModule { }
