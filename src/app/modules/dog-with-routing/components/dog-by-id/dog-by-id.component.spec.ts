import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DogByIdComponent } from './dog-by-id.component';

describe('DogByIdComponent', () => {
  let component: DogByIdComponent;
  let fixture: ComponentFixture<DogByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DogByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DogByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
