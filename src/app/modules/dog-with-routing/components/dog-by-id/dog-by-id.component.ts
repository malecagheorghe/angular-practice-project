import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DogService } from '../../services/dog.service';
import { DogType } from '../../types/DogType';

@Component({
  selector: 'app-dog-by-id',
  templateUrl: './dog-by-id.component.html',
  styleUrls: ['./dog-by-id.component.css']
})
export class DogByIdComponent implements OnInit {

  dog: DogType;

  constructor(private route: ActivatedRoute, private service: DogService) { }

  ngOnInit(): void {
    const dogName = this.route.snapshot.paramMap.get('name');
    this.dog = this.service.getDog(dogName);
  }

}
