import { Component, Input, OnInit } from '@angular/core';
import { DogType } from '../../types/DogType';
import { DogService } from '../../services/dog.service';

@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.css']
})
export class DogComponent implements OnInit {

  dogs: DogType[];

  public bark(name: String) {
    alert(name + ' barked');
  }

  public addDog(formInput: { name: String, age: Number }) {
    this.service.addDog(new DogType(formInput.name, formInput.age));
  }

  constructor(private service: DogService) { }

  ngOnInit(): void {
    this.dogs = this.service.getDogs();
  }
}
