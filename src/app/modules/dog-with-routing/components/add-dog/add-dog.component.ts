import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-dog',
  templateUrl: './add-dog.component.html',
  styleUrls: ['./add-dog.component.css']
})
export class AddDogComponent implements OnInit {

  @Output() addEvent: EventEmitter<any> = new EventEmitter<any>();

  public addDog(dog: NgForm) {
    this.addEvent.emit({ name: dog.value.name, age: dog.value.age });
  }

  constructor() { }

  ngOnInit(): void {
  }
}
