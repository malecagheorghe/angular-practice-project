import { Component, Input, OnInit } from '@angular/core';
import { DogType } from "../../types/DogType";

@Component({
  selector: 'app-presenter-child',
  templateUrl: './presenter-child.component.html',
  styleUrls: ['./presenter-child.component.css']
})
export class PresenterChildComponent implements OnInit {

  @Input() dog: DogType;

  constructor() { }

  ngOnInit(): void {
  }

}
