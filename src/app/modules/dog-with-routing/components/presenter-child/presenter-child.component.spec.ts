import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresenterChildComponent } from './presenter-child.component';

describe('PresenterChildComponent', () => {
  let component: PresenterChildComponent;
  let fixture: ComponentFixture<PresenterChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresenterChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresenterChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
