import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CounterComponent } from './counter/counter.component';
import { MicroServiceComponent } from './micro-service/micro-service.component';
import { HttpClientModule } from '@angular/common/http';
import { DogWithRoutingModule } from './modules/dog-with-routing/dog-with-routing.module';
import { MenuComponent } from './menu/menu.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DogByIdComponent } from './modules/dog-with-routing/components/dog-by-id/dog-by-id.component';

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    MicroServiceComponent,
    MenuComponent,
    NotFoundComponent,
    DogByIdComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DogWithRoutingModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
