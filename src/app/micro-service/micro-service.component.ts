import { NgForOf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ToDoType } from '../types/ToDoType';
import { RESTserviceService as Rest } from '../services/restservice.service';

@Component({
  selector: 'app-micro-service',
  templateUrl: './micro-service.component.html',
  styleUrls: ['./micro-service.component.css']
})
export class MicroServiceComponent implements OnInit {

  list: ToDoType[];


  constructor(private rest: Rest) { }

  ngOnInit(): void {
    this.rest
      .getToDos()
      .subscribe(res => this.list = res);
  }




}
