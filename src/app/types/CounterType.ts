import { Observer, Observable } from "rxjs";

export class CounterType {
  private counter: number;
  private observer: Observer<Number>;
  private observable: Observable<Number>;

  constructor() {
    this.counter = 0;
    this.observable = new Observable<Number>(
      subscriber => {
        this.observer = subscriber;
      }
    )
  }

  public getCounter(): Number {
    return this.counter;
  }

  public getObservable(): Observable<Number> {
    return this.observable;
  }

  public increment() {
    this.counter++;
    if (this.counter % 5 === 0) {
      this.observer.next(this.counter);
    }
    if (this.counter > 20) {
      this.observer.complete();
    }
  }

  public reset() {
    this.counter = 0;
  }
}
