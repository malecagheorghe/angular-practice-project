export class ToDoType {
  userId: Number;
  id: Number;
  title: String;
  finished: Boolean;

  constructo(
    userId: Number,
    id: Number,
    title: String,
    finished: Boolean
  ) {
    this.userId = userId;
    this.id = id;
    this.title = title;
    this.finished = finished;
  }

}
