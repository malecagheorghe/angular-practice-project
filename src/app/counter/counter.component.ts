import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { CounterType } from '../types/CounterType';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  currentNumber: Number;
  counter: CounterType;

  public increase() {
    this.counter.increment();
  }

  public reset() {
    this.counter.reset();
    this.counter.getObservable().subscribe(n => this.currentNumber = n);
  }

  constructor() {
    this.currentNumber = 0;
    this.counter = new CounterType();
    this.counter.getObservable().subscribe(n => this.currentNumber = n);
  }

  ngOnInit(): void {
  }
}
